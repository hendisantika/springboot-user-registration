# Spring Boot User Registration
## Things to do:
1. Clone repository: 
    ```
    git clone https://gitlab.com/hendisantika/springboot-user-registration.git 
    ```
2. Go to folder:
    ```
    cd springboot-user-registration
    ```
3. Run the application
    ```
    mvn clean spring-boot:run
    ```
4. Go to your favorite browser: http://localhost:8080
## Screen shot
### Home Page
![Home Page](img/home.png "Home Page")

### Register Page
![Register Page](img/register.png "Register Page")

### Success Page
![Successful Page](img/success.png "Successful Page")

### List Members Page
![List Members Page](img/list.png "List Members Page")