package com.hendisantika.springbootuserregistration.repository;

import com.hendisantika.springbootuserregistration.model.AppUser;
import com.hendisantika.springbootuserregistration.model.AppUserForm;
import com.hendisantika.springbootuserregistration.model.Gender;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-user-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-21
 * Time: 05:59
 */

@Repository
@RequiredArgsConstructor
public class AppUserDAO {
    private static final Map<Long, AppUser> USERS_MAP = new HashMap<>();

    static {
        initDATA();
    }

    // Config in WebSecurityConfig
    private final PasswordEncoder passwordEncoder;

    private static void initDATA() {
        String encryptedPassword = "";

        AppUser naruto = new AppUser(1L, "naruto", "Uzumaki", "Naruto", //
                true, Gender.MALE, "uzumaki_naruto@konohagakure.com", encryptedPassword, "JP");

        AppUser sasuke = new AppUser(2L, "sasuke", "Uchiha", "Sasuke", //
                true, Gender.MALE, "uchiha_sasuke@konohagakure.com", encryptedPassword, "JP");

        AppUser sakura = new AppUser(3L, "sakura", "Haruno", "Sakura", //
                true, Gender.MALE, "haruno_sakura@konohagakure.com", encryptedPassword, "JP");

        USERS_MAP.put(naruto.getUserId(), naruto);
        USERS_MAP.put(sasuke.getUserId(), sasuke);
        USERS_MAP.put(sakura.getUserId(), sakura);
    }

    public Long getMaxUserId() {
        long max = 0;
        for (Long id : USERS_MAP.keySet()) {
            if (id > max) {
                max = id;
            }
        }
        return max;
    }

    public AppUser findAppUserByUserName(String userName) {
        Collection<AppUser> appUsers = USERS_MAP.values();
        for (AppUser u : appUsers) {
            if (u.getUserName().equals(userName)) {
                return u;
            }
        }
        return null;
    }

    public AppUser findAppUserByEmail(String email) {
        Collection<AppUser> appUsers = USERS_MAP.values();
        for (AppUser u : appUsers) {
            if (u.getEmail().equals(email)) {
                return u;
            }
        }
        return null;
    }

    public List<AppUser> getAppUsers() {
        List<AppUser> list = new ArrayList<>();

        list.addAll(USERS_MAP.values());
        return list;
    }

    public AppUser createAppUser(AppUserForm form) {
        Long userId = this.getMaxUserId() + 1;
        String encryptedPassword = this.passwordEncoder.encode(form.getPassword());

        AppUser user = new AppUser(userId, form.getUserName(), //
                form.getFirstName(), form.getLastName(), false, //
                form.getGender(), form.getEmail(), form.getCountryCode(), //
                encryptedPassword);

        USERS_MAP.put(userId, user);
        return user;
    }

}
