package com.hendisantika.springbootuserregistration.repository;

import com.hendisantika.springbootuserregistration.model.Country;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-user-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-21
 * Time: 07:22
 */
@Repository
@RequiredArgsConstructor
public class CountryDAO {

    private static final Map<String, Country> COUNTRIES_MAP = new HashMap<>();

    static {
        initDATA();
    }

    private static void initDATA() {

        Country jp = new Country("JP", "Japan");
        Country en = new Country("EN", "England");
        Country fr = new Country("FR", "France");
        Country us = new Country("US", "US");
        Country ru = new Country("RU", "Russia");
        Country id = new Country("ID", "Indonesia");
        Country in = new Country("IN", "India");
        Country uk = new Country("UK", "United Kingdom");
        Country cn = new Country("CN", "China");
        Country my = new Country("MY", "Malaysia");

        COUNTRIES_MAP.put(jp.getCountryCode(), jp);
        COUNTRIES_MAP.put(en.getCountryCode(), en);
        COUNTRIES_MAP.put(fr.getCountryCode(), fr);
        COUNTRIES_MAP.put(us.getCountryCode(), us);
        COUNTRIES_MAP.put(ru.getCountryCode(), ru);
        COUNTRIES_MAP.put(id.getCountryCode(), id);
        COUNTRIES_MAP.put(in.getCountryCode(), in);
        COUNTRIES_MAP.put(uk.getCountryCode(), uk);
        COUNTRIES_MAP.put(cn.getCountryCode(), cn);
        COUNTRIES_MAP.put(my.getCountryCode(), my);
    }

    public Country findCountryByCode(String countryCode) {
        return COUNTRIES_MAP.get(countryCode);
    }

    public List<Country> getCountries() {
        List<Country> list = new ArrayList<>();
        list.addAll(COUNTRIES_MAP.values());
        return list;
    }

}
