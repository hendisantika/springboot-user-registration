package com.hendisantika.springbootuserregistration.model;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-user-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-21
 * Time: 05:56
 */
@Data
public class AppUser {
    private Long userId;
    private String userName;
    private String firstName;
    private String lastName;
    private boolean enabled;
    private String gender;
    private String email;
    private String encrytedPassword;
    private String countryCode;

    public AppUser(Long userId, String userName, String firstName, String lastName,
                   boolean enabled, String gender,
                   String email, String countryCode, String encrytedPassword) {
        super();
        this.userId = userId;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.enabled = enabled;
        this.gender = gender;
        this.email = email;
        this.countryCode = countryCode;
        this.encrytedPassword = encrytedPassword;
    }

}
