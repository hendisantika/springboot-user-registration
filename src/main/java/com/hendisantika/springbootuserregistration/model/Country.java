package com.hendisantika.springbootuserregistration.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-user-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-21
 * Time: 05:57
 */
@Data
@AllArgsConstructor
public class Country {
    private String countryCode;
    private String countryName;
}
