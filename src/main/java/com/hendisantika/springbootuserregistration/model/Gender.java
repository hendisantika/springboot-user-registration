package com.hendisantika.springbootuserregistration.model;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-user-registration
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-21
 * Time: 05:58
 */
public class Gender {
    public static final String MALE = "M";
    public static final String FEMALE = "F";
}
